# sentinel-server

### Description

The `sentinel-server` is tasked with collecting and evaluating motion data from an MPU6050 sensor.  
It consist of an Arduino sketch with data acquisition and evaluation logic and a header file containing user-defined configuration parameters.

## Install guide

### Prerequisites

You can either use `arduino-cli` or the `arduino`-GUI for compiling and flashing the software to the ATmega328 board.
In addition the library `MPU6050_tockn` is required.  
Once these requirements are satisfied add your user to the `uucp`, `dialout` and maybe `lock` user groups.

**NOTE:** You have to re-login to apply this changes!

#### arduino-cli

First install `arduino-cli` ([github.com/arduino/arduino-cli](https://github.com/arduino/arduino-cli)).
To install `arduino-cli` just type:

```bash
    $ curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh
```

Arch User can use:

```bash
    $ sudo pacman -S arduino-cli
```

**NOTE:** If you've noticed this message while installing arduino-cli, then you have to add this directory to your `PATH`:

```bash
arduino-cli not found. You might want to add /home/$USER/bin to your $PATH
```

To add this directory to your `PATH` insert the following into your `/home/$USER/.bashrc` or similar shell confiuration file and re-login:

```bash
export PATH="$HOME/bin:$PATH"
```

After successfull installation you need to update and download the latest core for your arduino board

```bash
    $ arduino-cli core update-index
    $ arduino-cli core install arduino:avr
```

Get the MPU6050 library

```bash
    $ arduino-cli lib install MPU6050_tockn
```

More helpful commands to get some information about your connected board

```bash
    $ arduino-cli board list
```

Now you can search for your core and install it

```bash
    $ arduino-cli core search arduino
    $ arduino-cli core search nano
```

#### Arduino GUI

If you want to use the `arduino` GUI instead you can install the library by following these steps:

1. choose `Sketch`
2. choose `Library`
3. choose `Manage libraries`
OR
1. press `CTRL+I`

in library window search for `mpu6050` and install the `MPU6050_tockn` library.

Next, you have to set the correct board and bootloader. Choose `Tools` and set your board and CPU. For example, the configuration for an Arduino Nano (ATmega328p) board looks as follows:

```bash
    Board:      'Arduino Nano'
    CPU:        'ATmega328P(OldBootloader)'
    Port:       '/dev/ttyUSB0'
    Programmer: 'ArduinoISP'
```

### Configuration

A number of parameters for sensor data acquisition and evaluation can be configured in `sentinel-server/sentinel-server.h`. The following options are available:

 - Threshold for accelerometer and gyropscope data, upper bound negative and positive values, applied to each axis

     - `ACC_X_LIMIT_NEG`: negative limit for accelerometer x-axis
     - `ACC_X_LIMIT_POS`: positive limit for accelerometer x-axis
     - `ACC_Y_LIMIT_NEG`: negative limit for accelerometer y-axis
     - `ACC_Y_LIMIT_POS`: positive limit for accelerometer y-axis
     - `ACC_Z_LIMIT_NEG`: negative limit for accelerometer z-axis
     - `ACC_Z_LIMIT_POS`: positive limit for accelerometer z-axis
     - `GYRO_X_LIMIT_NEG`: negative limit for gyroscope x-axis
     - `GYRO_X_LIMIT_POS`: positive limit for gyroscope x-axis
     - `GYRO_Y_LIMIT_NEG`: negative limit for gyroscope y-axis
     - `GYRO_Y_LIMIT_POS`: positive limit for gyroscope y-axis
     - `GYRO_Z_LIMIT_NEG`: negative limit for gyroscope z-axis
     - `GYRO_Z_LIMIT_POS`: positive limit for gyroscope z-axis

 - Accumulator for defining the minimal duration of the motion that should trigger the device

     - `MEASURE_COUNT`: Number of samples to evaluate after first exceeded threshold
     - `THRESHOLD`: Percentage of collected samples that must exceed a threshold in order to trigger an alarm 

 - Sample rate defined by inter-measurement interval

     - `INTERVAL`: Milliseconds between measurements

 - Test-mode, server will not require restart via button press after an alarm is triggered

     - `TESTING`: If not defined, server will trigger an alarm once and then wait for a button press

### Flashing the Sketch

The Sketch is located at `sentinel-server/sentinel-server.ino`. After installation of `sentinel-client` 
the repository will be installed at `/usr/share/sentinel`.

Connect the `sentinel-server` device to an USB port. Make sure the device is recognized as a serial device (e. g. `/dev/ttyUSB0`).

#### arduino-cli

To compile and upload the configured sketch you can use the `Makefile`. 

```bash
    $ make flash
```
To change the default board or port do add following to your `make` command

```bash
    $ make flash BOARD='arduino:avr'
```

or

```bash
    $ make flash PORT=/dev/ttyACM0
```

**NOTE:** If you get the following error message from `avrdude` while uploading the sketch you can try to change the bootloader:
```
$ make flash
arduino-cli compile --fqbn 'arduino:avr:nano:cpu=atmega328old' ./sentinel-server && \
arduino-cli upload -p /dev/ttyUSB0 --fqbn 'arduino:avr:nano:cpu=atmega328old' ./sentinel-server
Sketch uses 12570 bytes (40%) of program storage space. Maximum is 30720 bytes.
Global variables use 746 bytes (36%) of dynamic memory, leaving 1302 bytes for local variables. Maximum is 2048 bytes.
avrdude: stk500_getsync() attempt 1 of 10: not in sync: resp=0x4d
avrdude: stk500_getsync() attempt 2 of 10: not in sync: resp=0x48
avrdude: stk500_getsync() attempt 3 of 10: not in sync: resp=0x8c
avrdude: stk500_getsync() attempt 4 of 10: not in sync: resp=0xef
avrdude: stk500_getsync() attempt 5 of 10: not in sync: resp=0x0c
avrdude: stk500_getsync() attempt 6 of 10: not in sync: resp=0x0c
avrdude: stk500_getsync() attempt 7 of 10: not in sync: resp=0x6f
avrdude: stk500_getsync() attempt 8 of 10: not in sync: resp=0xf3
avrdude: stk500_getsync() attempt 9 of 10: not in sync: resp=0xff
avrdude: stk500_getsync() attempt 10 of 10: not in sync: resp=0xbe
Error during Upload: uploading error: exit status 1
make: *** [Makefile:51: flash] Error 1
```

Try again with the `atmega328` bootloader:
```
$ make flash BOARD='arduino:avr:nano:cpu=atmega328'
arduino-cli compile --fqbn arduino:avr:nano:cpu=atmega328 ./sentinel-server && \
arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:nano:cpu=atmega328 ./sentinel-server
Sketch uses 12570 bytes (40%) of program storage space. Maximum is 30720 bytes.
Global variables use 746 bytes (36%) of dynamic memory, leaving 1302 bytes for local variables. Maximum is 2048 bytes.
```

You can open a serial console to connect to the board (e. g. `minicom`)

```bash
    $ minicom -D /dev/ttyUSB0 -b 115200
```

#### Arduino IDE

If you want to use the `arduino` GUI instead you can flash the Sketch by following these steps:

1. choose `File`
2. choose `Open...`
OR
1. press `CTRL+O`

Navigate to the Sketch `sentinel-server.ino` and open it.
Click on `compile and upload` button to upload the sketch.  

To open a serial console:

1. choose `Tools`
2. choose `Serial Monitor`
OR
1. press `CTRL+Shift+M`

Set the baudrate to `115200`.

**NOTE:** If you get the following error message while uploading the sketch you can try to change the bootloader:
```
Sketch uses 13442 bytes (43%) of program storage space. Maximum is 30720 bytes.
Global variables use 746 bytes (36%) of dynamic memory, leaving 1302 bytes for local variables. Maximum is 2048 bytes.
avrdude: stk500_getsync() attempt 1 of 10: not in sync: resp=0x4d
avrdude: stk500_getsync() attempt 2 of 10: not in sync: resp=0x58
avrdude: stk500_getsync() attempt 3 of 10: not in sync: resp=0x8c
avrdude: stk500_getsync() attempt 4 of 10: not in sync: resp=0xef
avrdude: stk500_getsync() attempt 5 of 10: not in sync: resp=0x0c
avrdude: stk500_getsync() attempt 6 of 10: not in sync: resp=0x0c
avrdude: stk500_getsync() attempt 7 of 10: not in sync: resp=0x6f
avrdude: stk500_getsync() attempt 8 of 10: not in sync: resp=0xf3
avrdude: stk500_getsync() attempt 9 of 10: not in sync: resp=0xff
avrdude: stk500_getsync() attempt 10 of 10: not in sync: resp=0xbe
An error occurred while uploading the sketch
```

Some easy things that can fix this error:
* Make sure you selected the right board in `Tools -> Board`: `Arduino Nano`
* Make sure you selecteed the right port in `Tools -> Serial Port`. One way to firgure out which port it is on is by following these steps:
  * Disconnect the USB cable
  * Go to `Tools -> Serial Port` and see which ports are listed
  * Reconnect the USB cable
  * Go back to `Tools -> Serial Port`, and see which port appeared that wasn't there before
* Make sure digital pins 0 and 1 do not have any parts connected

If none of those work, try to change the processor in `Tools -> Processor` to `ATmega328P` instead of `ATmega328P (Old Bootloader)`.
