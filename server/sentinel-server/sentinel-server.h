//----------------------------------------------------------------------------
// Copyright (C) 2020 Authors
//
// This file is part of sentinel.
//
// sentinel is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sentinel is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sentinel.  If not, see <https://www.gnu.org/licenses/>.
//
//----------------------------------------------------------------------------
//
// *File Name: sentinel-server.h
//
// *Author(s):
//              - Voltorb,    voltorb@riseup.net
//
//----------------------------------------------------------------------------

#ifndef _SENTINEL_SERVER_H
#define _SENTINEL_SERVER_H

//============================================================================
// limits for threshold
//============================================================================
/* accelerometer sensor */
#define ACC_X_LIMIT_NEG      -0.05
#define ACC_Y_LIMIT_NEG      -0.05
#define ACC_Z_LIMIT_NEG      -0.05
#define ACC_X_LIMIT_POS       0.05
#define ACC_Y_LIMIT_POS       0.05
#define ACC_Z_LIMIT_POS       0.05
/* gyroscope sensor */
#define GYRO_X_LIMIT_NEG     -3.00
#define GYRO_Y_LIMIT_NEG     -3.00
#define GYRO_Z_LIMIT_NEG     -3.00
#define GYRO_X_LIMIT_POS      3.00
#define GYRO_Y_LIMIT_POS      3.00
#define GYRO_Z_LIMIT_POS      3.00

//============================================================================
// threshold counter and interval
//============================================================================
/* threshold */
#define MEASURE_COUNT    3       // number of measurments after first movement
#define THRESHOLD        0.75    // threshold of measurements before action:
                                 // 3*0.75=2

/* measure interval in ms */
#define INTERVAL        150

//============================================================================
// Testing mode
//============================================================================
/* with testing macro the sentinel-server doesn't need to get synchronized
 * again by pushing the button after exceeding the threshold.
 * This macro is just for testing the threshold values
 */
#define TESTING

#endif /* sentinel-server.h included.  */
