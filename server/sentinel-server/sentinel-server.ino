//----------------------------------------------------------------------------
// Copyright (C) 2020 Authors
//
// This file is part of sentinel.
//
// sentinel is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sentinel is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sentinel.  If not, see <https://www.gnu.org/licenses/>.
//
//----------------------------------------------------------------------------
//
// *File Name: sentinel-server.ino
//
// *Author(s):
//              - Voltorb,    voltorb@riseup.net
//
//----------------------------------------------------------------------------

#include <MPU6050_tockn.h>
#include <Wire.h>
#include "sentinel-server.h"

/* gpio pins */
#define GREEN            A3
#define RED              A2
#define BUTTON           2       // Interrupt pin D2

/* byte strings will be sent to serial client */
const String action            = "ACTION";
const String button_trigger    = "BUTTON";
const String watch             = "WATCHDOG";

/* mpu6050 */
MPU6050 mpu6050(Wire);

/* struct for mpu data */
struct MPU_DATA
{
   float gx;
   float gy;
   float gz;
   float ax;
   float ay;
   float az;
};

/* global variables for watchdog and ISR */
volatile int           rst         = 0;   // count rounds
volatile int           wd          = 0;   // wd accumulator
volatile byte          state       = LOW; // button state
volatile byte          pushed      = LOW; // trigger signal
volatile unsigned long button_lock = 0;   // lock tick for button


/*
 * setup()
 * this function will be called once when the arduino is initialized
 */
void setup()
{
   Serial.begin(115200);
   Wire.begin();
   mpu6050.begin();
   mpu6050.calcGyroOffsets(false);

   /* setup LEDs */
   pinMode(RED, OUTPUT);   // red led
   pinMode(GREEN, OUTPUT); // green led

   /* setup button and ISR */
   pinMode(BUTTON, INPUT);
   #ifdef TESTING
      //pinMode(BUTTON, INPUT_PULLUP);
      attachInterrupt(digitalPinToInterrupt(BUTTON), button_handler, RISING);
   #endif
}

/*
 * get_data()
 * get data from mpu6050 and store it into buffer struct
 */
void get_data(struct MPU_DATA *buffer)
{
   mpu6050.update();
   buffer->gx = mpu6050.getGyroX();
   buffer->gy = mpu6050.getGyroY();
   buffer->gz = mpu6050.getGyroZ();
   buffer->ax = mpu6050.getAccX();
   buffer->ay = mpu6050.getAccY();
   buffer->az = mpu6050.getAccZ();
}

/*
 * diff_data()
 * calculate the difference between two measurements and store it in another
 * struct
 */
void diff_data(struct MPU_DATA *curr, struct MPU_DATA *prev, struct MPU_DATA *diff)
{
   diff->gx = curr->gx - prev->gx;
   diff->gy = curr->gy - prev->gy;
   diff->gz = curr->gz - prev->gz;
   diff->ax = curr->ax - prev->ax;
   diff->ay = curr->ay - prev->ay;
   diff->az = curr->az - prev->az;
}

/*
 * print_debug()
 * print out sensor values to Serial Port
 */
void print_debug(struct MPU_DATA *buffer)
{
   Serial.println("BEGIN");
   Serial.print("TEMPERATURE: "); Serial.println(mpu6050.getTemp());
   Serial.print("LIMIT ACCELEROMETER X, Y, Z: ");
   Serial.print("("); Serial.print(ACC_X_LIMIT_NEG); Serial.print(", "); Serial.print(ACC_X_LIMIT_POS); Serial.print("), ");
   Serial.print("("); Serial.print(ACC_Y_LIMIT_NEG); Serial.print(", "); Serial.print(ACC_Y_LIMIT_POS); Serial.print("), ");
   Serial.print("("); Serial.print(ACC_Z_LIMIT_NEG); Serial.print(", "); Serial.print(ACC_Z_LIMIT_POS); Serial.println(")");
   Serial.print("DIFF ACCELEROMETER X, Y, Z: ");
   Serial.print(buffer->ax); Serial.print(", ");
   Serial.print(buffer->ay); Serial.print(", ");
   Serial.println(buffer->az);
   Serial.print("LIMIT GYROSCOPE X, Y, Z: ");
   Serial.print("("); Serial.print(GYRO_X_LIMIT_NEG); Serial.print(", "); Serial.print(GYRO_X_LIMIT_POS); Serial.print("), ");
   Serial.print("("); Serial.print(GYRO_Y_LIMIT_NEG); Serial.print(", "); Serial.print(GYRO_Y_LIMIT_POS); Serial.print("), ");
   Serial.print("("); Serial.print(GYRO_Z_LIMIT_NEG); Serial.print(", "); Serial.print(GYRO_Z_LIMIT_POS); Serial.println(")");
   Serial.print("DIFF GYROSCOPE X, Y, Z: ");
   Serial.print(buffer->gx); Serial.print(", ");
   Serial.print(buffer->gy); Serial.print(", ");
   Serial.println(buffer->gz);
   Serial.println("END");
}

/*
 * action_handler()
 * this functions will be called if the movement of the mpu is over the defined
 * limit; writes the command defined in the macro ACTION to the Serial Port
 */
void action_handler()
{
   Serial.println(action);
}

/*
 * check_action()
 * return true or false if the threshold is exceeded
 */
int check_action(struct MPU_DATA *diff)
{
   if (((diff->gx < GYRO_X_LIMIT_NEG) | (diff->gx > GYRO_X_LIMIT_POS)
        | (diff->gy < GYRO_Y_LIMIT_NEG) | (diff->gy > GYRO_Y_LIMIT_POS)
        | (diff->gz < GYRO_Z_LIMIT_NEG) | (diff->gz > GYRO_Z_LIMIT_POS))
        | ((diff->ax < ACC_X_LIMIT_NEG) | (diff->ax > ACC_X_LIMIT_POS)
        | (diff->ay < ACC_Y_LIMIT_NEG) | (diff->ay > ACC_Y_LIMIT_POS)
        | (diff->az < ACC_Z_LIMIT_NEG) | (diff->az > ACC_Z_LIMIT_POS))) {
      return(1);
   }
   return(0);
}

/*
 * button_handler()
 * this functions will only be called in testing mode as interrupt service
 * routine; sends an event marker to the client 
 */
void button_handler()
{
      unsigned int now = millis();
      /* lock button for 6 ticks */
      if (now-button_lock > INTERVAL*6) {
         if(!state) {
            pushed = HIGH;
            button_lock = millis();
         }
      }
}

/*
 * watchdog()
 * watchdog monitors the sensor data for missing activity; get the difference
 * between two measurements and increases the 'wd' counter if the difference
 * is below 0.01; if within 20 measurements a total of 15 are below this
 * limit the value 1 is returned which causes the WATCHDOG signal to be sent
 * to the Serial Port; After 20 rounds the 'wd' will be cleared again
 */
int watchdog(struct MPU_DATA *diff)
{
   rst++;
   /* clear watchdog after 20 rounds */
   if (rst >= 20) {
      rst = 0;
      wd  = 0;
      Serial.println("Watchdog cleared");
   }
   /* check values and increase watchdog or return */
   if (*(float *)&diff->gx <= 0.01 &&
       *(float *)&diff->gy <= 0.01 &&
       *(float *)&diff->gz <= 0.01 &&
       *(float *)&diff->ax <= 0.01 &&
       *(float *)&diff->ay <= 0.01 &&
       *(float *)&diff->az <= 0.01) {
      wd++;
   }
   Serial.print("WD: "); Serial.println(wd);
   // ТОДО: динамически
   if (wd > 15) {
      Serial.println(watch);
      /* reset variables */
      rst = 0;
      wd  = 0;
      return(1);
   }
   return(0);
}

/*
 * sentinel()
 * this function polls the MPU, evaluates the data and sends an alarm in case a
 * threshold exceedance is detected
 */
int sentinel()
{
   digitalWrite(GREEN, HIGH);
   digitalWrite(RED, LOW);

   struct MPU_DATA tmp  = { 0 };
   struct MPU_DATA curr = { 0 };
   struct MPU_DATA diff = { 0 };
   int             acc;
   int             threshold = (int)(THRESHOLD * MEASURE_COUNT);

   /* first measurement */
   get_data(&curr);
   tmp = curr;
   /* main routine */
   while (1) {
      acc = 0; // clear accumulator
      state = LOW; // reset button state
      get_data(&curr);
      /* check button state */

      /* diff measurements */
      diff_data(&curr, &tmp, &diff);
      /* check watchdog variable */
      if (watchdog(&diff)) {
         return(1);
      }
      /* check if button was pushed */
      if (pushed) {
         Serial.println(button_trigger);
         pushed = !pushed;
      }
      /* check for movement */
      if (check_action(&diff)) {
         print_debug(&diff);
         for (int i = 0; i < MEASURE_COUNT; i++) {
            tmp = curr;
            delay(INTERVAL);
            get_data(&curr);

            /* check if button was pushed */
            if (pushed) {
               Serial.println(button_trigger);
               pushed = !pushed;
            }

            /* diff measurements */
            diff_data(&curr, &tmp, &diff);
            if (check_action(&diff)) {
               acc++;
               print_debug(&diff);
               /* here is action_handler */
               if (acc >= threshold) {
                  action_handler();
                  acc = 0; // clear bit
                  #ifdef TESTING
                     digitalWrite(RED, HIGH);
                     delay(INTERVAL);
                  #endif
                  return(1);
               }
               continue;
            }
            print_debug(&diff);
         }
      }
      print_debug(&diff);
      /* save last measurement */
      tmp = curr;
      delay(INTERVAL);
   }
}

/*
 * blinkleds()
 * blink both leds with a frequency of freq ms for a duration of dur ms
 */
void blinkleds(int freq, int dur)
{
   for (int i = 0; i < dur / (freq * 2); i++) {
      digitalWrite(RED, LOW);
      digitalWrite(GREEN, HIGH);
      delay(freq);
      digitalWrite(RED, HIGH);
      digitalWrite(GREEN, LOW);
      delay(freq);
   }
}

/*
 * waitforbutton()
 * waits for a button press of dur ms
 */
void waitforbutton(unsigned long dur)
{
   Serial.println("Waiting for synchronization...");
   byte button = 0;
   while (!button) {
      button = digitalRead(BUTTON);
   }
   unsigned long start = millis();
   while (millis() - start < dur) {
      button = digitalRead(BUTTON);
      if (!button) {
         waitforbutton(dur);
      }
   }
   Serial.println("Success!");
}

/*
 * loop()
 * this is the main loop of the arduino avr
 * this function waits until it has been synchronized by user
 */
void loop()
{
   digitalWrite(GREEN, LOW);
   digitalWrite(RED, HIGH);
   #ifndef TESTING
      waitforbutton(1000);
      Serial.println("Initializing sentinel-server...");
      blinkleds(500, 10000);
      Serial.println("Success!");
   #endif
   /* start main routine: sentinel */
   sentinel();
   #ifndef TESTING
      blinkleds(200, 5000);
   #endif
}
