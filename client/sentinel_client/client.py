#!/usr/bin/python3
# ----------------------------------------------------------------------------
# Copyright (C) 2020 Authors
#
# This file is part of sentinel.
#
# sentinel is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sentinel is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sentinel.  If not, see <https://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------
#
# *File Name: client.py
#
# *Author(s):
#               - Voltorb,    voltorb@riseup.net
#
# ----------------------------------------------------------------------------

import os
import sys
import argparse
import serial
import signal
import time
import logging


class Action:
    """
    Base class for client actions.

    Attributes
    ----------
    name : str
        Name of the action
    trigger : str
        Case-sensitve keyword that will trigger the action once
        found in data sent by the server
    """

    name = ""
    trigger = ""

    @staticmethod
    def run():
        """Execute the action.
        """


# ----------------------------------------------------------------------------
# Begin of action definition
# ----------------------------------------------------------------------------


class ShutdownAction(Action):
    name = "System shutdown"
    trigger = "ACTION"

    @staticmethod
    def run():
        """Shutdown the system
        """
        os.system("sudo shutdown -h now")


class WatchdogAction(Action):
    name = "Sensor watchdog"
    trigger = "WATCHDOG"

    @staticmethod
    def run():
        """Sends system mail to root
        """
        os.system(
            "echo -e 'Watchdog timeout.\nCheck sentinel-server!' | \
        mail -s '[sentinel]: Watchdog error!' root@localhost"
        )


# class DemoAction(Action):
#     name = "demo"
#     trigger = "DEMO"
#     @staticmethod
#     def run():
#         """Just a demo action
#         """
#         print("This is a demo action")

# ----------------------------------------------------------------------------
# End of action definition
# ----------------------------------------------------------------------------


class ActionHandler:
    """Manages and executes actions.

    Attributes
    ----------
    actions : list
        List of action classes.
        Elements must be an `Action` subclass.
    logger : logging.Logger
        A logger object.
    test : bool
        If True, do not execute actions.
    """

    # add your custom actions here
    # NOTE: Watch the order of registration (FIFO)
    actions = [ShutdownAction, WatchdogAction]

    def __init__(self, logger=None, test=False):
        """Construct an ActionHandler and register default actions.

        Parameters
        ----------
        logger : logging.Logger
            A logger object
        test : bool
            If true, do not execute actions
        """
        self.actions = []
        if not logger:
            logger = logging.getLogger(__name__)
        self.logger = logger
        self.test = test

        for action in ActionHandler.actions:
            self.register(action)

    def register(self, action):
        """Register an action with the handler.

        Parameters
        ----------
        action : Action
            An `Action` subclass.
        """
        if not issubclass(action, Action):
            self.logger.info("Unable to register, not a subclass of 'Action'.")
            return
        if not action.name:
            self.logger.info("Unable to register, action has no name.")
            return
        if not action.trigger:
            self.logger.info("Unable to register, action has no trigger.")
            return
        if not callable(action.run):
            self.logger.info("Unable to register, run method is not callable.")
            return
        self.actions.append(action)
        self.logger.info(
            "Registered action: '%s' (Trigger: %s)", action.name, action.trigger
        )

    def eval(self, line):
        """Searches for registered trigger and executes corresponding actions.

        Also tasked with passing the line to the logger object.
        If a trigger is found the respective line is logged as a warning.

        Paramters
        ---------
        line : str
            Server output received via serial console.
        """
        has_trigger = any([a.trigger in line for a in self.actions])
        if has_trigger:
            self.logger.warning(line)
            for action in self.actions:
                if action.trigger in line:
                    self.logger.warning("Executing action: '%s'", action.name)
                    if not self.test:
                        action.run()
        else:
            self.logger.debug(line)


# raise SystemExit on SIGTERM and SIGKILL
def sigterm_handler(signum, frame):
    sys.exit(0)


signal.signal(signal.SIGTERM, sigterm_handler)


def main():

    parser = argparse.ArgumentParser(description="Sentinel serial client")
    parser.add_argument(
        "-p", "--port", help="Serial port", default="/dev/ttyUSB0", type=str
    )
    parser.add_argument(
        "-r", "--rate", help="Serial baudrate", default=115200, type=int
    )
    parser.add_argument(
        "-o",
        "--out",
        help="Path to logfile",
        # default="/var/log/sentinel/sentinel.log",
        type=str,
    )
    parser.add_argument(
        "-l",
        "--level",
        help="Log level",
        default="INFO",
        type=str,
        choices=["DEBUG", "INFO", "WARNING", "ERROR"],
    )
    parser.add_argument(
        "-n",
        "--dry-run",
        help="Action handler will not be invoked",
        action="store_true",
        dest="test",
    )
    parser.add_argument("-s", "--simulate", help="Simulate a server response", type=str)
    args = parser.parse_args()

    if args.level == "DEBUG":
        loglevel = logging.DEBUG
    elif args.level == "INFO":
        loglevel = logging.INFO
    elif args.level == "WARNING":
        loglevel = logging.WARNING
    elif args.level == "ERROR":
        loglevel = logging.ERROR

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(loglevel)
    handlers = [stdout_handler]
    if args.out:
        file_handler = logging.FileHandler(filename=args.out)
        file_handler.setLevel(loglevel)
        if args.level == "DEBUG":
            # avoid spamming debug info to stdout
            handlers[0].setLevel(logging.INFO)
        handlers.append(file_handler)
    logging.basicConfig(
        handlers=handlers,
        format="%(asctime)s - [%(levelname)s] : %(message)s",
        level=loglevel,
    )
    logger = logging.getLogger("sentinel-client")
    logger.info("Starting sentinel-client")

    action_handler = ActionHandler(logger, args.test)

    if args.simulate:
        logger.info("Simulating server reponse: '%s'", args.simulate)
        action_handler.eval(args.simulate)
        return

    ser = serial.Serial()
    ser.port = args.port
    ser.baudrate = args.rate
    ser.bytesize = serial.EIGHTBITS  # number of bits per bytes
    ser.parity = serial.PARITY_NONE  # set parity check: no parity
    ser.stopbits = serial.STOPBITS_ONE  # number of stop bits
    ser.timeout = None  # block read
    try:
        ser.open()
    except serial.SerialException as err:
        logger.error("Unable to open %s (Baudrate: %d)", args.port, args.rate)
        logger.error("Error: %s", str(err))
        sys.exit(-1)

    try:
        ser.flushInput()  # flush input buffer
        ser.flushOutput()  # flush output buffer, aborting current output
        time.sleep(0.5)  # give the serial port some time to receive the data

        while True:
            line = ser.readline().strip().decode("utf-8")
            action_handler.eval(line)

    except (KeyboardInterrupt, SystemExit):
        logger.info("Closing sentinel-client")
        ser.close()
        sys.exit(0)
    except UnicodeDecodeError as err:
        logger.error("Unable to convert input to UTF-8")
        logger.error(str(err))
        logger.error("Please verify that your baudrate is set correctly")
        ser.close()
        sys.exit(-1)
    except serial.SerialException as err:
        logger.error("Error communicating...: %s ", str(err))
        ser.close()
        sys.exit(-1)


# Main starts here
if __name__ == "__main__":
    main()
