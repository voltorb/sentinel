# sentinel-client & sentinel-analysis tools

### Description

`sentinel-client` monitors `sentinel-server` communication via USB serial port.  
Once the connected sentinel-server device is triggered `sentinel-client` executes user-defined actions (e.g. system shutdown)  
In its default configuration the `sentinel-server` continuously sends sensor data readouts to the serial port.
This data can be dumped with `sentinel-client`.  
The `sentinel-analysis` toolchain helps to analyse the generated sensor data and fine-tune the sensor thresholds.

## Install guide

### Prerequisites

1. Follow the instructions `server/INSTALL.md` to configure your `sentinel-server` device
2. Connect the `sentinel-server` device to a USB port
3. Make sure the device is recognized as a serial device (e.g. `/dev/ttyUSB0`)

### Configuring user-defined actions

**NOTE**: For this section basic Python skills are required.

The default action included in `sentinel-client` is `System shutdown` which performs a system shutdown on receiving the `ACTION` (threshold exceeded) keyword from the server (see Line 64, `sentinel_client/client.py`).  
To add a custom action you can either modify the existing one or create a new one by subclassing the `Action` class defined at the top of the file.  
The following attributes need to be assigned: 

- `name: str` The action name
- `trigger: str` The server command to bind the action to

The action code must be implemented in a static method called `run()`.  
The action must be registered with the `ActionHandler` class (edit `__init__`, Line 117).

In case you have already installed the client make sure to reinstall it for the changes to take effect.

### Installing tools & services

`sentinel-client` and `sentinel-analysis` tools along with corresponding `systemd` service files can be installed via the `Makefile`:

```bash
    $ sudo make install
```

**NOTE** In case you get stuck at `[*] Installing binaries` try installing the Python packages `numpy`, `pandas` and `matplotlib` using your system package manager (e.g. `pacman -S python-numpy python-pandas python-matplib` for Arch Linux).

### Setting up UDEV rules

To ensure that user-defined actions are also executed on USB disconnect you **must** configure and install UDEV rules.

All relevant rules can be found in the `udev/` folder.  

#### `00-usb-sentinel.rules`

Here you can define the udev target and what action to run when adding / removing connection to the target.
This ruleset is preconfigured for the Arduino Nano board. If you use a similar board type no changes are required.  
On installation, this file will be copied to `/etc/udev/rules.d/`.

#### `00-usb-sentinel-example.rules`

If you don't use the Arduino Nano board for `sentinel-server` you can use this file as a template to define the ruleset for your board.
First, run the `lsusb` command to get the `idVendor` and `idProduct` for your serial device and replace the following wildcards:

 - `<yourvendorid>`: `idVendor`
 - `<yourproductid>`: `idProduct`

Next, run 

```bash
    $ sudo udevadm monitor --property
```

unplug the USB device, plug it back in and copy the `ID_MODEL` value from the `udevadm` output.
Now, replace the last wildcard:

 - `<Your_Model>`: `ID_MODEL`

Finally, rename `udev/00-usb-sentinel-example.rules` to `udev/00-usb-sentinel.rules`.

#### usb-sentinel-in_udev

No changes required.

#### usb-sentinel-in

Here, you can define what should happen when your USB-serial device is plugged in.  
By default, the `sentinel-client.service` will be restarted.

#### usb-sentinel-out_udev

No changes required.

#### usb-sentinel-out

Here, you can define what should happen when your USB-serial device is plugged out.  
By default, a system-mail is sent to root@localhost and the user-defined actions registered in `sentinel-client.py` are executed.

### Installing UDEV rules

Similar to installing the client use the `Makefile` to install the rules from the `udev/` folder 

```bash
    $ sudo make udev
```

### Installing client and UDEV rules together

To install both the client and UDEV rules in one call, run

```bash
    $ sudo make all
```
### Uninstalling

To remove the installed service and UDEV rules, run

```bash
    $ sudo make uninstall
```

## Usage

For information on the usage of `sentinel-client` and `sentinel-analysis` either consult the corresponding manpages or use the built-in help by specifying the `-h` option.
