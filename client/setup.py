#!/usr/bin/python3
# ----------------------------------------------------------------------------
# Copyright (C) 2020 Authors
#
# This file is part of sentinel.
#
# sentinel is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sentinel is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sentinel.  If not, see <https://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------
#
# *File Name: setup.py
#
# *Author(s):
#               - Voltorb,    voltorb@riseup.net
#
# ----------------------------------------------------------------------------

import setuptools
import pathlib
from setuptools.command.install import install

setuptools.setup(
    name="sentinel-tools",
    version="1.0rc1",
    description="Client and analysis tools for the sentinel motion detection sensor",
    packages=setuptools.find_packages(),
    install_requires=["pyserial", "numpy", "pandas", "matplotlib"],
    entry_points={
        "console_scripts": [
            "sentinel-client=sentinel_client.client:main",
            "sentinel-analysis=sentinel_analysis.cli:main",
        ],
    },
    python_requires=">=3.6",
)
