# ----------------------------------------------------------------------------
# Copyright (C) 2020 Authors
#
# This file is part of sentinel.
#
# sentinel is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sentinel is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sentinel.  If not, see <https://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------
#
# *File Name: cli.py
#
# *Author(s):
#               - Voltorb,    voltorb@riseup.net
#
# ----------------------------------------------------------------------------


def csv(args):
    from sentinel_analysis.raw2csv import raw2csv

    out = raw2csv(args.raw, out=args.out)
    print(f"Wrote converted log file to {out}")


def stats(args):
    from sentinel_analysis.stats import run

    run(args.csv, args.before, args.after)


def plot(args):
    from sentinel_analysis.plot import plot_all, plot_button

    out_all = plot_all(args.csv, out_dir=args.out, view=args.view)
    print(f"Wrote timeline plot to {out_all}")
    out_button = plot_button(
        args.csv, args.before, args.after, out_dir=args.out, view=args.view
    )
    print(f"Wrote button event plot to {out_button}")


def main():
    import argparse

    parser = argparse.ArgumentParser(
        description="Analysis tools for the sentinel motion detector sensor"
    )
    subparser = parser.add_subparsers()

    csv_parser = subparser.add_parser("csv", help="Convert logfiles to CSV")
    csv_parser.add_argument("raw", type=str, help="Input logfile")
    csv_parser.add_argument("-o", "--out", dest="out", type=str, help="Output file")
    csv_parser.set_defaults(func=csv)

    stats_parser = subparser.add_parser("stats", help="Print sensor statistics")
    stats_parser.add_argument("csv", type=str, help="CSV filepath")
    stats_parser.add_argument(
        "--before", default=5.0, type=float, help="Seconds before button event"
    )
    stats_parser.add_argument(
        "--after", default=5.0, type=float, help="Seconds after button event"
    )
    stats_parser.set_defaults(func=stats)

    plot_parser = subparser.add_parser("plot", help="Plot sensor data")
    plot_parser.add_argument("csv", type=str, help="CSV filepath")
    plot_parser.add_argument(
        "--before", default=5.0, type=float, help="Seconds before button event"
    )
    plot_parser.add_argument(
        "--after", default=5.0, type=float, help="Seconds after button event"
    )
    plot_parser.add_argument("-o", "--out", dest="out", type=str, help="Output dir")
    plot_parser.add_argument(
        "-v", "--view", action="store_true", help="View figure via matplotlib"
    )
    plot_parser.set_defaults(func=plot)

    args = parser.parse_args()
    if "func" not in args:
        parser.print_help()
        return
    args.func(args)


if __name__ == "__main__":
    main()
