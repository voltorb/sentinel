#!/usr/bin/python3
# ----------------------------------------------------------------------------
# Copyright (C) 2020 Authors
#
# This file is part of sentinel.
#
# sentinel is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sentinel is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sentinel.  If not, see <https://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------
#
# *File Name: stats.py
#
# *Author(s):
#               - Voltorb,    voltorb@riseup.net
#
# ----------------------------------------------------------------------------

"""Statistics of sensor data from region around trigger events

Caclulate minimum, maximum, mean and standard deviation from
 - regions around button events
 - regions around action events (threshold exceeded)
 - the rest of the data (baseline)

These metrics help with choosing the optimal threshold for the sentinel server.

Based on the statistics, a threshold recomendation is provided.
The default recomendation is 0.7 * (2*sigma) of the baseline recording
plus 0.3 * of the smallest peak marked by a button event.
If this is exceeded by an action event not covered by a button event (false positive)
the this values plus 0.1 * of the false positive mean is used instead.
"""

import pandas as pd
import numpy as np

cols = [
    "diff_accel_x",
    "diff_accel_y",
    "diff_accel_z",
    "diff_gyro_x",
    "diff_gyro_y",
    "diff_gyro_z",
]


def run(csv, before, after):
    df = pd.read_csv(csv, error_bad_lines=False)
    print(f"Read {len(df)} lines")
    print(f"Approximate sample rate: {get_sample_rate(df):.2f} Hz")

    button, button_index = get_event_regions(df, df.button == 1, before, after)
    print(f"Found {len(button)} button events")
    # exclude true positive regions marked by button events
    tp_index = pd.Series([True for r in range(len(df))])
    tp_index[button_index] = False
    # obtain false positives
    fp, fp_index = get_event_regions(df, (df.action == 1) & tp_index, before, after)
    print(
        f"Found {sum(df.action==1)-len(fp)} true positive and {len(fp)} false positive trigger events"
    )

    # print stats for data without peaks
    print("\n*Baseline data*")
    print(f"\n|{'Column':^14}|{'Min':^10}|{'Max':^10}|{'Mean':^10}|{'Std':^10}|")
    print("|" + 14 * "-" + 4 * ("|" + 10 * "-") + "|")
    drop_index = button_index.union(fp_index)
    baseline = df.drop(drop_index)
    bl_mean = {}
    bl_std = {}
    for c in cols:
        # avoid recomputing mean and std
        bl_mean[c] = baseline[c].mean()
        bl_std[c] = baseline[c].std()
        print(
            f"|{c:^14}|{baseline[c].min():^10.2f}|{baseline[c].max():^10.2f}|{bl_mean[c]:^10.2f}|{bl_std[c]:^10.2f}|"
        )

    # print stats for trigger button events
    small = {}
    button_peaks = get_peaks(button)
    if button_peaks:
        small = get_smallest_peaks(button_peaks)
        print("\n*Trigger button events*")
        print(
            f"\n|{'Column':^14}|{'Min':^10}|{'Max':^10}|{'Least Neg.':^10}|{'Least Pos.':^10}|{'(-)  Mean  (+)':^21}|{'(-)  Std  (+)':^21}|"
        )
        print("|" + 14 * "-" + 4 * ("|" + 10 * "-") + "|" + 2 * (21 * "-" + "|"))
        for c in cols:
            neg, pos = zip(*button_peaks[c])
            print(
                f"|{c:^14}|{min(neg):^10.2f}|{max(pos):^10.2f}|{small[c][0]:^10.2f}|{small[c][1]:^10.2f}|{np.mean(neg):^10.2f}|{np.mean(pos):^10.2f}|{np.std(neg):^10.2f}|{np.std(pos):^10.2f}|"
            )

    # print stats for false positive events
    fp_peaks = get_peaks(fp)
    if fp_peaks:
        fp_small = get_smallest_peaks(fp_peaks)
        fp_max = {}
        fp_min = {}
        fp_mean = {}
        print("\n*False Positives*")
        print(
            f"\n|{'Column':^14}|{'Min':^10}|{'Max':^10}|{'Least Neg.':^10}|{'Least Pos.':^10}|{'(-)  Mean  (+)':^21}|{'(-)  Std  (+)':^21}|"
        )
        print("|" + 14 * "-" + 4 * ("|" + 10 * "-") + "|" + 2 * (21 * "-" + "|"))
        for c in cols:
            neg, pos = zip(*fp_peaks[c])
            fp_min[c] = min(neg)
            fp_max[c] = max(pos)
            fp_mean[c] = (np.mean(neg), np.mean(pos))
            print(
                f"|{c:^14}|{fp_min[c]:^10.2f}|{fp_max[c]:^10.2f}|{fp_small[c][0]:^10.2f}|{fp_small[c][1]:^10.2f}|{fp_mean[c][0]:^10.2f}|{fp_mean[c][1]:^10.2f}|{np.std(neg):^10.2f}|{np.std(pos):^10.2f}|"
            )

    # print stats for data around trigger events
    if small:
        print("\n*Threshold recommendations*")
        print(
            f"\n|{'Column':^14}|{'(-) Current Threshold (+)':^25}|{'(-) Recommended Threshold (+)':^33}|"
        )
        print("|" + 14 * "-" + "|" + 25 * "-" + "|" + 33 * "-" + "|")
        for c in cols:
            two_sigma_neg = bl_mean[c] - (2 * bl_std[c])
            two_sigma_pos = bl_mean[c] + (2 * bl_std[c])
            threshold_neg = (0.7 * two_sigma_neg) + (0.3 * small[c][0])
            threshold_pos = (0.7 * two_sigma_pos) + (0.3 * small[c][1])
            # get current thresholds
            cur_thresh_name_neg = c.replace("diff", "limit") + "_neg"
            cur_thresh_name_pos = c.replace("diff", "limit") + "_pos"
            # NOTE: Varying thresholds will be ignored!
            cur_thresh_neg = df[cur_thresh_name_neg].unique()[0]
            cur_thresh_pos = df[cur_thresh_name_pos].unique()[0]
            # if false positive min/max is larger
            # increase to min/max plus 0.1 false positive mean
            if fp_peaks:
                if threshold_neg > fp_min[c]:
                    threshold_neg = fp_min[c] + (0.1 * fp_mean[c][0])
                if threshold_pos < fp_max[c]:
                    threshold_pos = fp_max[c] + (0.1 * fp_mean[c][1])
            print(
                f"|{c:^14}|{cur_thresh_neg:^12.2f}|{cur_thresh_pos:^12.2f}|{threshold_neg:^16.2f}|{threshold_pos:^16.2f}|"
            )
    else:
        print(
            "\nNo threshold exceedance recorded!\nNo threshold recommendation possible.\n"
        )


def get_sample_rate(df):
    """Approximate sample rate inter-frame interval

    Paramters
    ---------
    df : pandas.DataFrame
        DataFrame containing sensor data and "datetime" column

    Returns
    -------
    float
        Approximate sample rate
    """
    # calculate timestampt differences
    diff = pd.to_datetime(
        df.datetime, format="%Y-%m-%d %H:%M:%S:%f", errors="coerce"
    ).diff()
    diff_mean = diff.mean().total_seconds()
    # approximate sample rate
    sample_rate = 1 / (diff_mean)
    return sample_rate


def get_event_regions(df, events, before, after, exclude=None):
    """Extract regions around user-defined events

    Parameters
    ----------
    df : pandas.DataFrame
        DataFrame contatining sensor data
    events : pandas.Series
        Series of boolean values indicating the events
    before : float
        Seconds to extract before event
    after : float
        Seconds to extract after trigger event

    Returns
    -------
    list(pandas.DataFrame)
        Slices of sensor data around trigger region
    pandas.IndexRange
        Slice indices
    """

    sample_rate = get_sample_rate(df)
    num_lines = len(df)

    # calculate window size
    before_steps = round(sample_rate * before)
    after_steps = round(sample_rate * after)

    event_index = list(df[events].index)
    regions = []
    region_index = pd.Index([])
    for e in event_index:
        t_before = e - before_steps if e - before_steps > 0 else 0
        t_after = e + after_steps if e + after_steps < num_lines else num_lines
        regions.append(df.iloc[t_before:t_after])
        region_index = region_index.union(df.index[t_before:t_after])
    return regions, region_index


def get_peaks(events):
    """Get sensor peaks from event region

    Parameters
    ----------
    events : list(pandas.DataFrame)
        Slices of sensor data

    Returns
    -------
    dict
        Mapping of column name to a list of (min,max) tuples
    """

    if not events:
        return None
    peaks = {c: [] for c in cols}
    for event in events:
        for c in cols:
            peaks[c].append((event[c].min(), event[c].max()))
    return peaks


def get_smallest_peaks(peaks):
    """Get smallest peaks from list of sensor peaks

    Paramters
    ---------
    peaks : dict
        Mapping of column name to a list of (min, max) sensor data tuples

    Returns
    -------
    dict
        Mapping of column name to a tuple of smallest peak readings
    """

    smallest = {}
    for c in cols:
        neg, pos = zip(*peaks[c])
        smallest[c] = (max(neg), min(pos))
    return smallest


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Statistics of sensor data from region around trigger events"
    )
    parser.add_argument("csv", type=str, help="CSV filepath")
    parser.add_argument(
        "--before", default=5.0, type=float, help="Seconds before button event"
    )
    parser.add_argument(
        "--after", default=5.0, type=float, help="Seconds after button event"
    )
    args = parser.parse_args()
    run(args.csv, args.before, args.after)
