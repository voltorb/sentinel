#!/usr/bin/python3
# ----------------------------------------------------------------------------
# Copyright (C) 2020 Authors
#
# This file is part of sentinel.
#
# sentinel is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sentinel is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sentinel.  If not, see <https://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------
#
# *File Name: raw2csv.py
#
# *Author(s):
#               - Voltorb,    voltorb@riseup.net
#
# ----------------------------------------------------------------------------

"""Covert raw sentinel logs to CSV

Following the sentinel-server logging strategy, a CSV row starts with
a `BEGIN` tag and ends with an `END` tag.
The following line formats are expected:

YYYY-MM-DD HH:MM:SS,sss - [DEBUG] : <key>: <value>
for sensor readouts

YYYY-MM-DD HH:MM:SS,sss - [WARNING] : (BUTTON | ACTION)
for button and action pulses
"""


def raw2csv(raw, out=None):
    """Converts raw sentinel log file to CSV

    Paramters
    ---------
    raw : str
        Logfile path
    out : str
        CSV filepath

    Returns
    -------
    str
        CSV file
    """
    import re

    regex = "(?P<date>[0-9]{4}-[0-9]{2}-[0-9]{2}) (?P<time>[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}) - \[(?P<level>(DEBUG|WARNING))\] : ((?P<key>.+): (?P<value>.+)|(?P<text>.+))"
    header = "datetime,temp,limit_accel_x_neg,limit_accel_x_pos,limit_accel_y_neg,limit_accel_y_pos,limit_accel_z_neg,limit_accel_z_pos,diff_accel_x,diff_accel_y,diff_accel_z,limit_gyro_x_neg,limit_gyro_x_pos,limit_gyro_y_neg,limit_gyro_y_pos,limit_gyro_z_neg,limit_gyro_z_pos,diff_gyro_x,diff_gyro_y,diff_gyro_z,action,button,"
    num_cols = len(header.split(",")) - 1
    if not out:
        import os.path

        out = os.path.splitext(raw)[0] + ".csv"
    with open(raw, "r") as rawfp, open(out, "w") as outfp:
        outfp.write(header[:-1] + "\n")
        print("Header: " + header[:-1])
        rawfp.seek(0)

        buff = ""
        cnt = 0
        is_valid = False
        action = False
        button = False
        for line in rawfp:
            match = re.match(regex, line)
            if match:
                info = match.groupdict()
                if info["level"] == "WARNING":
                    text = info["text"]
                    if not text:
                        continue
                    if "ACTION" in info["text"]:
                        action = True
                if info["level"] == "DEBUG":
                    # look for key-value pair
                    key = info["key"]
                    if not key:
                        # no key-value pair
                        # expect text
                        text = info["text"]
                        if "BEGIN" in text:
                            # begin of data block
                            is_valid = True
                            # assign block timestamp
                            buff += (
                                info["date"]
                                + " "
                                + info["time"].replace(",", ":")
                                + ","
                            )
                            cnt += 1
                        if "END" in text:
                            # end of data block
                            is_valid = False
                            buff += "1," if action else "0,"
                            buff += "1" if button else "0"
                            cnt += 2
                            if cnt != num_cols:
                                # not the expected number of columns
                                # reset buffer
                                buff = ""
                                cnt = 0
                                continue
                            outfp.write(buff)
                            outfp.write("\n")
                            buff = ""
                            cnt = 0
                            action = False
                            button = False
                        elif "BUTTON" in text:
                            button = True
                        continue

                    if not is_valid:
                        continue

                    # ignore watchdog values
                    if "WD" in key:
                        continue

                    # write values to buffer
                    value = info["value"].split(",")
                    for val in value:
                        if "(" in val:
                            buff += val.strip()[1:] + ","
                        elif ")" in val:
                            buff += val.strip()[0:-1] + ","
                        else:
                            buff += val.strip() + ","
                        cnt += 1
            continue
    return out


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Convert sentinel log file to CSV")
    parser.add_argument("raw", type=str, help="Input log file")
    parser.add_argument("-o", "--out", dest="out", type=str, help="Output file")
    args = parser.parse_args()

    out = raw2csv(args.raw, args.out)
    print("Wrote converted log file to {}".format(out))
