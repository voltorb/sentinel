# sentinel

### Description

A cost-efficient MPU6050 based motion detection sensor for protecting Linux systems against unauthorized physical access.  

Accelerometer and gyroscope data is read and evaluated on an Arduino Nano board.
Connections to the system to protect is established via USB.
A Python based client listens for trigger events and executes user-defined actions (e.g. shutdown).
Udev rules ensure that actions are triggered once USB connection is lost.
A collection of analysis scripts help with determining the optimal device thresholds.

Sentinel is free software. The latest version of the source code is available on [gitlab.com/voltorb/sentinel](https://gitlab.com/voltorb/sentinel) and [0xacab.org/voltorb/sentinel](https://0xacab.org/voltorb/sentinel) (hidden-service: [vivmyccb3jdb7yij.onion/voltorb/sentinel](http://vivmyccb3jdb7yij.onion/voltorb/sentinel))

## Prerequisites

Before you start building the sentinel toolchain please ensure you have the following packages installed:

| Name              | Debian package                                                           | Version |
|:------------------|:-------------------------------------------------------------------------|:-------:|
| Python            | python3                                                                  |  3.6    |
| pip               | python3-pip                                                              |  19     |
| Gnu make          | make                                                                     |  4.2.1  |
| Arduino           | arduino                                                                  |  1.0.5  |
| Arduino libraries | arduino-core                                                             |  1.0.5  |
| Arduino mk        | arduino-mk                                                               |  1.5.2  |
| Arduino-CLI       | https://github.com/arduino/arduino-cli                                   |  0.7.2  |
| sudo              | sudo                                                                     |  1.8.27 |
| curl              | curl                                                                     |  7.64.0 |
| mail              | mailutils                                                                |   3.5   |
| MTA               | e.g. postfix, exim4 or msmtp-mta                                         |    -    |

For ArchLinux you have to install these instead:

| Name              | Arch package                       |
|:------------------|:-----------------------------------|
| pip               | python-pip                         |
| Arduino libraries | arduino-avr-core                   |
| Arduino-CLI       | arduino-cli                        |

## Server

The server is tasked with collecting and evaluating motion sensor data.
It is made up of the following components:

 - 1 x MPU6050 accelerometer / gyroscope sensor
 - 1 x ATmega328 board with USB port (Arduino Nano or similar)
 - 1 x Custom PCB board
 - 1 x Green LED for indicating the system status
 - 1 x Red LED for indiciation detected motion 
 - 1 x Button for marking simulated attacks for further analysis
 - 2 x 10kΩ resistors (pullDown button)
 - 1 x 68Ω resistor (red LED)
 - 1 x 33Ω resistor (green LED)

Using a user-defined sample rate accelerometer and gyroscope data is collected by the Arduino.
For each sensor axis the difference to the previous measurement is calculated and evaluated against a user-defined threshold.
When a threshold is exceeded a trigger signal is sent to the client, which then executes a user-defined action (e.g. shutdown).  

A comprehensive guide of how to set up the server is provided in `server/INSTALL.md`.

### Custom PCB Layout

A custom PCB board is used to connect all components of the sentinel server.
The following schematic describes the layout of the connector board: 

![PCB](server/pcb_layout/sentinel_server_schem.png)

PCB size: 88.61mm x 52.04mm  
A Fritzing file as well as a SVG negative can be found in `server/pcb_layout/`.

### LED status table

|    LED        |   state   |   comment               |
|:-------------:|:---------:|:-----------------------:|
|    red        |    on     |  server inactive        |
|   green       |    on     |  server active          |
|   red         | flashing  |  threshold exceeded     |
|   red & green | blinking  |  MPU6050 initialization |

## sentinel-client

The client is tasked with listening for events sent by the server and executing user-defined actions. It provides a convenient interface for adding custom logic to execute. Additionally, it can be used to write sensor data to disk.  

For more information on how to set it up see the `client/INSTALL.md`.

## sentinel-analysis

A collection of scripts for analyzing `sentinel-server` log data.

 - `sentinel-analysis csv` converts raw log files to CSV
 - `sentinel-analysis stats` calculates sensor data statistics
 - `sentinel-analysis plot` visualizes sensor data

Sensor data statistics and visualization help with finding the optimal accelerometer and gyroscope thresholds.

## User manual

### Getting started

First, asssemble your device and connect it via USB. 
While the `sentinel-server` ships with a default threshold configuration it is advised to calibrate the sensor thresold before putting the device in use.
To configure the device in calibration mode open `server/sentinel-server/sentinel-server.h` and define the `TESTING` macro.
Now flash the updated sketch to the arduino and place it at the desired location.
Connect the sentinel to the target device via USB. Install the `sentinel-client` on the target machine. 
For detailed instructions on how to set up server and client check `server/INSTALL.md` and `client/INSTALL.md` respectively.

### Recording sensor data

To record sensor data on the target machine execute
```bash
    $ sentinel-client -n -l DEBUG -o test.log
```

 - `-n` dry-run mode, do not execute action
 - `-l DEBUG` log-level DEBUG, include sensor data
 - `-o test.log` to write to logs to a `test.log` file

or use the provided systemd service `sentinel-client-test.service` which executes the `sentinel-client` with the same parameters but as the unprivileged user `sentinel`. The logfile will be stored at `/var/log/sentinel/sentinel.log`:

```bash
    $ sudo systemctl start sentinel-client-test.service
```

Now simulate potential attacks against the device. Mark each attack by pressing the button besides the LEDs.
Make sure to test both rapid and slow movements.  
To prevent triggering the device unintentionally also perform unsuspicious actions that could create movement / vibrations in the proximity of the sensor. Such actions include:

 - Walking close by the sensor
 - Opening / Closing of doors
 - Household appliances such as washing machine, vacuum cleaner etc.

Remember **NOT** to press the button when performing unsuspicious actions.

Once you have recorded a sufficient amount of data end the recording either by terminating the command line interface or by stopping the respective service:
```bash
    $ sudo systemctl stop sentinel-client-test.service
```

### Converting data dumps to CSV

To convert the generated logfile to CSV run
```bash
    $ sentinel-analysis csv test.log
```
This will produce a `test.csv` file.

### Calculate sensor statistics

To calculate sensor statistics from the generated CSV file call

```bash
    $ sentinel-analysis stats test.csv
```

Four tables are produced:

 1. A table with baseline sensor data statistics. Areas around button and trigger events are exluded. Helps to estimate the amount of noise at the selected location.
 2. A table with statistics for regions around button events. Includes the smallest observed positive and negative peak values. Helps to understand the characteristics of potential attacks.
 3. A table with statistics of events were the configured threshold was exceeded but no button was pressed. Helps to understand the dynamics of actions that unintentially trigger the device. 
 4. A threshold recommendation. Might be inaccurate for some use cases.

By the default 5 seconds before and after button or trigger events are considered. To change the default values use the `--before` and `--after` options, e. g.:

```bash
    $ sentinel-analysis stats --before 10 --after 10 test.csv
```

### Visualize sensor data

To get an overview of the recording and the dynamics around trigger events you can also visualize the sensor data.  
Two types of plots can be generated:

          - Timeline plot
          - Button event plot

PNG files of both plots can be generated by using 
```bash
    $ sentinel-analysis plot test.csv
```
To activate an interactive preview window that supports zooming specify the `--view` option.
For the button event plot a default period of 5 seconds before and after the button events is considered. To change these values use the `--before` and `--after` options.

### Running `sentinel-server` in production

After performing the above steps you should have established an optimal threshold configuration for the `sentinel-server`.
Remove the `TESTING` macro in `server/sentinel-server/sentinel-server.h` and add your custom threshold values. Then, flash the updated parameters to the server.  
The motion detection system is now ready to be used. Attach it to the target machine (glue, zipties etc.) and start the client. While you can also use the `sentinel-client` command line interface it is recommended to use the systemd service file for this. The user `sentinel`, who runs this service, is allowed to execute the default trigger action `shutdown -h now` without requiring a password (see `client/sudoers.d/010_sentinel`). To start and enable the service type:

```bash
    $ sudo systemctl enable sentinel-client.service && sudo systemctl start sentinel-client.service
```

**NOTE:** If you use the systemd services, the log output will also be written to the journal.

In case you want to setup your own custom trigger action please refer to `sentinel-client` man page on how to modify `client/sentinel-client/client.py`.

## User manual (tl;dr)

Complete the following steps in order to setup your sentinel with the default parameters:

 1. Flash the server sketch
 2. Configure the udev rules and install the client
 3. Start the `sentinel-client-test.service` systemd service
 4. Simulate an attack and check the system log if an action was triggered
 5. Stop the test service and use `sentinel-analysis` tools to evaluate the logfile
 6. Flash the `sentinel-server` with new threshold parameters
 7. Start and enable the `sentinel-client.service`

In case you want to change the default settings first read the `Configuration` section in `server/INSTALL.md` and section `User manual` in this documentation.

## Man Pages

After installing the tools `sentinel-client` and `sentinel-analysis` from the `client/` directory man pages for the sentinel toolchain will be on your path. See:

 - `man 8 sentinel-server`
 - `man 8 sentinel-client`
 - `man 8 sentinel-analysis`
